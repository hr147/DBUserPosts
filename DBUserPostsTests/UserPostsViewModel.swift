//
//  UserPostsViewModel.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 17.02.23.
//

@testable import DBUserPosts
import XCTest

final class UserPostsViewModelTests: XCTestCase {
    private var sut: UserPostsViewModel!
    private var postsMock: UserPostsUseCaseMock!
    private var favouriteMock: FavouriteUseCaseMock!
    private let userId = "2"

    override func setUp() async throws {
        try await super.setUp()
        postsMock = .init()
        favouriteMock = .init()
        sut = await .init(userId: userId, useCase: postsMock, favouriteUseCase: favouriteMock)
    }

    override func tearDown() async throws {
        sut = nil
        postsMock = nil
        favouriteMock = nil
        try await super.tearDown()
    }

    func testRefreshPosts_whenServerReturnSuccess_shallReturnPosts() async {
        // Given
        postsMock.fetchPostsResult = .success([.mock()])
        let expectedPost = PostViewModel.mock()

        // When
        await sut.refreshPosts()

        // Then
        let posts = await sut.posts
        XCTAssertEqual(posts.count, 1)
        XCTAssertEqual(postsMock.userId, userId)
        XCTAssertEqual(posts.first, expectedPost)
    }

    func testRefreshPosts_whenServerReturnSuccessWithEmpty_shallReturnAlert() async {
        // Given
        postsMock.fetchPostsResult = .success([])
        let expectedAlert = AlertState(title: "", message: "Posts_Empty_Message")
        // When
        await sut.refreshPosts()

        // Then
        let posts = await sut.posts
        let alert = await sut.alert

        XCTAssertEqual(posts.count, 0)
        XCTAssertEqual(postsMock.userId, userId)
        XCTAssertEqual(alert, expectedAlert)
    }

    func testRefreshPosts_whenServerReturnFailure_shallReturnAlert() async {
        // Given
        let error = URLError(.badURL)
        postsMock.fetchPostsResult = .failure(error)
        let expectedAlert = AlertState(title: "", message: .init(error.localizedDescription))
        // When
        await sut.refreshPosts()

        // Then
        let posts = await sut.posts
        let alert = await sut.alert

        XCTAssertEqual(posts.count, 0)
        XCTAssertEqual(postsMock.userId, userId)
        XCTAssertEqual(alert, expectedAlert)
    }

    func testShowFavouritePosts() async {
        // Given
        favouriteMock.favourite = true
        postsMock.fetchPostsResult = .success([.mock()])
        await sut.refreshPosts()

        // When
        await sut.showFavouritePosts()

        // Then
        let favouritePosts = await sut.favouritePosts
        let showAll = await sut.showAll

        XCTAssertFalse(showAll)
        XCTAssertEqual(favouritePosts.count, 1)
    }

    func testShowAllPosts() async {
        // When
        await sut.showAllPosts()

        // Then
        let showAll = await sut.showAll

        XCTAssertTrue(showAll)
    }
}
