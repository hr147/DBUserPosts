//
//  PostViewModel+Mock.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 17.02.23.
//

@testable import DBUserPosts

extension PostViewModel {
    static func mock(
        post: Post = .mock(),
        useCase: FavouriteUseCase = FavouriteUseCaseMock()
    ) -> PostViewModel {
        .init(post, useCase: useCase)
    }
}

extension PostViewModel: Equatable {
    public static func == (lhs: PostViewModel, rhs: PostViewModel) -> Bool {
        lhs.id == rhs.id &&
            lhs.title == rhs.title &&
            lhs.body == rhs.body &&
            lhs.isFavourite == rhs.isFavourite
    }
}
