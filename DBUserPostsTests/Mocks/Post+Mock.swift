//
//  Post+Mock.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 16.02.23.
//

@testable import DBUserPosts

extension Post {
    static func mock(
        id: Int = 1,
        userId: Int = 12,
        title: String = "post.title",
        body: String = "post.body"
    ) -> Self {
        .init(id: id, userId: userId, title: title, body: body)
    }
}
