//
//  UserPostsUseCaseMock.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 17.02.23.
//

@testable import DBUserPosts

final class UserPostsUseCaseMock: UserPostsUseCase {
    private(set) var userId = ""
    var fetchPostsResult: Result<[Post], Error> = .success([])

    func fetchPosts(forUserId userId: String) async throws -> [Post] {
        self.userId = userId

        switch fetchPostsResult {
        case let .success(posts):
            return posts
        case let .failure(failure):
            throw failure
        }
    }

    private(set) var postId = ""
    var fetchPostCommentsResult: Result<[Comment], Error> = .success([])

    func fetchPostComments(withPostId postId: String) async throws -> [Comment] {
        self.postId = postId

        switch fetchPostCommentsResult {
        case let .success(comments):
            return comments
        case let .failure(failure):
            throw failure
        }
    }
}
