//
//  FavouriteUseCaseMock.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 16.02.23.
//

@testable import DBUserPosts

final class FavouriteUseCaseMock: FavouriteUseCase {
    private(set) var saveDidCall = false
    func save(_: Post) {
        saveDidCall = true
    }

    private(set) var removeDidCall = false
    func remove(_: Post) {
        removeDidCall = true
    }

    var favourite = false
    func isFavourite(_: Post) -> Bool {
        favourite
    }
}
