//
//  LoginViewModelTests.swift
//  DBUserPostsTests
//
//  Created by hrasheed on 17.02.23.
//

@testable import DBUserPosts
import SwiftUI
import XCTest

final class LoginViewModelTests: XCTestCase {
    func testLoginDidTap_whenTextIsEmpty_shallReturnAlert() async {
        // Given
        let sut = await LoginViewModel(state: .constant(.login))
        let expectedAlert = AlertState(title: "Invalid", message: "Please enter valid user id.")

        // When
        await sut.loginDidTap()

        // Then
        let currentAlert = await sut.alert
        XCTAssertEqual(currentAlert, expectedAlert)
    }
}
