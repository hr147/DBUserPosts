//
//  PostViewModelTests.swift
//  PostViewModelTests
//
//  Created by hrasheed on 14.02.23.
//

@testable import DBUserPosts
import XCTest

final class PostViewModelTests: XCTestCase {
    private var mock: FavouriteUseCaseMock!
    private var sut: PostViewModel!

    override func setUp() {
        super.setUp()
        mock = .init()
        sut = .init(.mock(), useCase: mock)
    }

    override func tearDown() {
        sut = nil
        mock = nil
        super.tearDown()
    }

    func testFavourite_whenFavouriteToggledFromFalse_shouldHaveSavedValue() {
        // Given
        mock.favourite = false

        // When
        sut.isFavourite.toggle()

        // Then
        XCTAssertTrue(mock.saveDidCall)
        XCTAssertFalse(mock.removeDidCall)
    }

    func testFavourite_whenFavouriteToggledFromTrue_shouldHaveRemovedValue() {
        // Given
        mock.favourite = true
        sut = .init(.mock(), useCase: mock)

        // When
        sut.isFavourite.toggle()

        // Then
        XCTAssertFalse(mock.saveDidCall)
        XCTAssertTrue(mock.removeDidCall)
    }

    func testViewModel_shouldHaveValidValues() {
        // Given
        let expected = PostViewModel.mock()

        // Then
        XCTAssertEqual(sut, expected)
    }
}
