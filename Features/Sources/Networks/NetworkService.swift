import Foundation

public protocol NetworkServiceProtocol {
    func request<T: Decodable>(with endpoint: Endpoint) async throws -> T
}

open class NetworkService {
    private let session: URLSession

    public init(session: URLSession = .shared) {
        self.session = session
    }

    deinit {
        session.invalidateAndCancel()
    }

    private func makeURLRequest(with endpoint: Endpoint) throws -> URLRequest {
        var components = URLComponents()
        components.scheme = endpoint.scheme
        components.host = endpoint.baseURL
        components.path = endpoint.path
        components.queryItems = endpoint.queryItems.isEmpty ? nil : endpoint.queryItems

        guard let url = components.url else {
            throw URLError(.badURL)
        }

        return URLRequest(url: url)
    }
}

extension NetworkService: NetworkServiceProtocol {
    enum NetworkServiceError: Error {
        case invalidResponse
        case unacceptableStatusCode(Int)
    }

    public func request<T: Decodable>(with endpoint: Endpoint) async throws -> T {
        let request = try makeURLRequest(with: endpoint)
        let (data, response) = try await session.data(for: request)

        guard let httpResponse = response as? HTTPURLResponse else {
            throw NetworkServiceError.invalidResponse
        }

        // Validate status code
        guard 200 ... 300 ~= httpResponse.statusCode else {
            throw NetworkServiceError.unacceptableStatusCode(httpResponse.statusCode)
        }

        return try endpoint.decoder.decode(T.self, from: data)
    }
}
