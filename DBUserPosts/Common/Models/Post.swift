//
//  Post.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Foundation

struct Post: Decodable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
}
