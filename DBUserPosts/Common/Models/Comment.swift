//
//  Comment.swift
//  DBUserPosts
//
//  Created by hrasheed on 17.02.23.
//

import Foundation

struct Comment: Decodable, Identifiable {
    let postId: Int
    let id: Int
    let name: String
    let email: String
    let body: String
}
