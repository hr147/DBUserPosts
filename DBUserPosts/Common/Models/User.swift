//
//  User.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Foundation

struct User {
    var id: String
    var isLoggedIn: Bool

    init(id: String, isLoggedIn: Bool = false) {
        self.id = id
        self.isLoggedIn = isLoggedIn
    }
}
