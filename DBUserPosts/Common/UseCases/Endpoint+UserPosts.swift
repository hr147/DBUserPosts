//
//  Endpoint+UserPosts.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Networks

extension Endpoint {
    static let baseUrl = "jsonplaceholder.typicode.com"

    static func posts(userId: String) -> Self {
        .init(
            baseURL: baseUrl,
            path: "/posts",
            queryItems: [
                .init(name: "userId", value: userId),
            ]
        )
    }

    static func comments(postId: String) -> Self {
        .init(
            baseURL: baseUrl,
            path: "/comments",
            queryItems: [
                .init(name: "postId", value: postId),
            ]
        )
    }
}
