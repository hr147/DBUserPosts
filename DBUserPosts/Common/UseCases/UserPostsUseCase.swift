//
//  UserPostsUseCase.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Foundation
import Networks

protocol UserPostsUseCase {
    func fetchPosts(forUserId userId: String) async throws -> [Post]
    func fetchPostComments(withPostId postId: String) async throws -> [Comment]
}

final class RemoteUserPostsUseCase: UserPostsUseCase {
    private let service: NetworkServiceProtocol

    init(service: NetworkServiceProtocol = NetworkService()) {
        self.service = service
    }

    func fetchPosts(forUserId userId: String) async throws -> [Post] {
        try await service.request(with: .posts(userId: userId))
    }

    func fetchPostComments(withPostId postId: String) async throws -> [Comment] {
        try await service.request(with: .comments(postId: postId))
    }
}
