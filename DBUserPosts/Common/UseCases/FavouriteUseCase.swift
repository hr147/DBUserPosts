//
//  FavouriteUseCase.swift
//  DBUserPosts
//
//  Created by hrasheed on 16.02.23.
//

import Foundation

protocol FavouriteUseCase {
    func save(_ post: Post)
    func remove(_ post: Post)
    func isFavourite(_ post: Post) -> Bool
}

final class FavouriteLocalUseCase {
    private static let key = "db.users.posts."

    private let userDefaults: UserDefaults

    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }

    private func fetchAllIds(with post: Post) -> [String]? {
        userDefaults.stringArray(forKey: Self.key + "\(post.userId)")
    }

    private func saveAllIds(_ ids: [String]?, with post: Post) {
        userDefaults.set(ids, forKey: Self.key + "\(post.userId)")
        userDefaults.synchronize()
    }
}

extension FavouriteLocalUseCase: FavouriteUseCase {
    func save(_ post: Post) {
        // post should not be saved already.
        guard isFavourite(post) == false else {
            return
        }

        // fetch stored posts id otherwise create empty array
        var ids = fetchAllIds(with: post) ?? []
        ids.append("\(post.id)")
        saveAllIds(ids, with: post)
    }

    func remove(_ post: Post) {
        // post should not be removed already.
        guard isFavourite(post) else {
            return
        }

        var ids = fetchAllIds(with: post)
        ids?.removeAll { post.id == Int($0) }
        saveAllIds(ids, with: post)
    }

    func isFavourite(_ post: Post) -> Bool {
        let ids = fetchAllIds(with: post)
        return ids?.contains(where: { post.id == Int($0) }) == true
    }
}
