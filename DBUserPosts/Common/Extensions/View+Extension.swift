//
//  View+Extension.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import SwiftUI

struct AlertState: Equatable {
    let title: LocalizedStringKey
    let message: LocalizedStringKey
}

extension View {
    /// Returns a type-erased version of `self`.
    func eraseToAnyView() -> AnyView {
        .init(self)
    }

    func errorAlertState(state: Binding<AlertState?>) -> some View {
        alert(isPresented: .constant(state.wrappedValue != nil)) {
            let cancelAction = {
                state.wrappedValue = nil
            }

            guard let title = state.wrappedValue?.title, let message = state.wrappedValue?.message else {
                return .init(title: Text(""), message: Text("something went wrong."), dismissButton: .cancel(cancelAction))
            }

            return Alert(title: Text(title), message: Text(message), dismissButton: .cancel(cancelAction))
        }
    }
}
