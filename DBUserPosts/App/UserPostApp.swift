//
//  DBUserPostsApp.swift
//  DBUserPosts
//
//  Created by hrasheed on 14.02.23.
//

import SwiftUI

@main
struct UserPostApp: App {
    @State var appState: AppState = .login

    var body: some Scene {
        WindowGroup {
            switch appState {
            case .login:
                LoginView(viewModel: .init(userId: "", state: $appState))

            case let .userPosts(userId):
                UserPostsView(viewModel: .init(userId: userId))
            }
        }
    }
}

enum AppState: Equatable {
    case login
    case userPosts(userId: String)
}
