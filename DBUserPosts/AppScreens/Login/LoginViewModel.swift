//
//  LoginViewModel.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Foundation
import struct SwiftUI.Binding

@MainActor
final class LoginViewModel: ObservableObject {
    @Published var userId: String
    @Published var alert: AlertState?
    @Binding var state: AppState

    init(userId: String = "", state: Binding<AppState>) {
        self.userId = userId
        _state = state
    }

    func loginDidTap() {
        guard userId.isEmpty == false else {
            alert = .init(title: "Invalid", message: "Please enter valid user id.")
            return
        }

        state = .userPosts(userId: userId)
    }
}
