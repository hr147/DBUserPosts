//
//  LoginView.swift
//  DBUserPosts
//
//  Created by hrasheed on 14.02.23.
//

import SwiftUI

struct LoginView: View {
    @StateObject private var viewModel: LoginViewModel

    init(viewModel: LoginViewModel) {
        _viewModel = .init(wrappedValue: viewModel)
    }

    var body: some View {
        VStack {
            userIdView()

            loginButton()
        }
        .padding()
        .errorAlertState(state: $viewModel.alert)
    }

    fileprivate func loginButton() -> some View {
        Button {
            viewModel.loginDidTap()
        } label: {
            Text("Login_Button_Title")
                .font(.title)
                .foregroundColor(Color.white)
                .frame(maxWidth: .infinity)
        }
        .buttonStyle(.borderedProminent)
        .padding(40)
    }

    fileprivate func userIdView() -> some View {
        HStack {
            Text("Login_User_Title")
                .font(.headline)

            VStack {
                TextField("", text: $viewModel.userId)
                    .keyboardType(.numberPad)

                Divider()
                    .frame(height: 1)
                    .background(Color.blue)
            }
        }
    }
}

#if DEBUG
    struct LoginView_Previews: PreviewProvider {
        static var previews: some View {
            LoginView(viewModel: .init(state: .constant(.login)))
        }
    }
#endif
