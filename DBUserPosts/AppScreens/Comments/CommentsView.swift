//
//  CommentsView.swift
//  DBUserPosts
//
//  Created by hrasheed on 17.02.23.
//

import SwiftUI

struct CommentsView: View {
    @ObservedObject var viewModel: CommentsViewModel

    init(viewModel: CommentsViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        List(viewModel.comments) { comment in
            VStack(alignment: .leading, spacing: 8) {
                Text(comment.name)
                    .font(.headline)

                Text(comment.body)
                    .font(.subheadline)
            }
        }
        .safeAreaInset(edge: .top) {
            PostView(viewModel: viewModel.post)
                .padding()
                .background(Color.white)
        }
        .task {
            await viewModel.refreshComments()
        }
        .refreshable {
            await viewModel.refreshComments()
        }
        .errorAlertState(state: $viewModel.alert)
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarTitle("Comment_Screen_Title")
    }
}
