//
//  CommentsViewModel.swift
//  DBUserPosts
//
//  Created by hrasheed on 17.02.23.
//

import Foundation

@MainActor
final class CommentsViewModel: ObservableObject {
    @Published var post: PostViewModel
    @Published var comments: [Comment]
    @Published var alert: AlertState?

    private let useCase: UserPostsUseCase

    init(comments: [Comment] = [], postViewModel: PostViewModel, useCase: UserPostsUseCase) {
        post = postViewModel
        self.useCase = useCase
        self.comments = comments
    }

    func refreshComments() async {
        do {
            comments = try await useCase.fetchPostComments(withPostId: post.id)

            if comments.isEmpty {
                alert = .init(title: "", message: "Comment_Empty_Message")
                return
            }

        } catch {
            alert = .init(title: "", message: .init(error.localizedDescription))
        }
    }
}
