//
//  UserPostsView.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import SwiftUI

struct UserPostsView: View {
    @StateObject var viewModel: UserPostsViewModel

    init(viewModel: UserPostsViewModel) {
        _viewModel = .init(wrappedValue: viewModel)
    }

    var body: some View {
        NavigationView {
            List(viewModel.showAll ? viewModel.posts : viewModel.favouritePosts) { post in
                NavigationLink {
                    CommentsView(viewModel: viewModel.makeCommentViewModel(with: post))
                } label: {
                    PostView(viewModel: post)
                }
            }
            .safeAreaInset(edge: .bottom) {
                filterView()
            }
            .task {
                await viewModel.refreshPosts()
            }
            .refreshable {
                await viewModel.refreshPosts()
            }
            .errorAlertState(state: $viewModel.alert)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarTitle("Posts_Screen_Title")
        }
    }

    fileprivate func filterView() -> some View {
        HStack {
            Button("Filter_Favourite_Button_Title") { viewModel.showAllPosts() }
                .frame(maxWidth: .infinity)

            Spacer()

            Button("Filter_All_Button_Title") { viewModel.showFavouritePosts() }
                .frame(maxWidth: .infinity)
        }
        .frame(maxWidth: .infinity)
        .padding(.vertical)
        .background(Color.blue)
        .foregroundColor(Color.white)
    }
}

#if DEBUG
    struct UserPostsView_Previews: PreviewProvider {
        static var previews: some View {
            UserPostsView(viewModel: .init(userId: ""))
        }
    }
#endif
