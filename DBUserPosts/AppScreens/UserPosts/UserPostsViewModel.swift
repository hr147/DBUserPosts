//
//  UserPostsViewModel.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Combine
import Foundation

@MainActor final class UserPostsViewModel: ObservableObject {
    @Published private(set) var posts: [PostViewModel]
    @Published private(set) var showAll = true
    @Published var alert: AlertState?

    var favouritePosts: [PostViewModel] {
        posts.filter(\.isFavourite)
    }

    private var fetchPostCancel = Set<AnyCancellable>()
    private let userId: String
    private let postsUseCase: UserPostsUseCase
    private let favouriteUseCase: FavouriteUseCase

    init(
        posts: [PostViewModel] = [],
        userId: String,
        useCase: UserPostsUseCase = RemoteUserPostsUseCase(),
        favouriteUseCase: FavouriteUseCase = FavouriteLocalUseCase()
    ) {
        self.posts = posts
        self.userId = userId
        postsUseCase = useCase
        self.favouriteUseCase = favouriteUseCase
    }

    func refreshPosts() async {
        do {
            let fetchedPosts = try await postsUseCase.fetchPosts(forUserId: userId)

            if fetchedPosts.isEmpty {
                alert = .init(title: "", message: "Posts_Empty_Message")
                return
            }

            fetchPostCancel.removeAll()
            posts = fetchedPosts.map { makePostViewModel(with: $0) }
        } catch {
            alert = .init(title: "", message: .init(error.localizedDescription))
        }
    }

    private func makePostViewModel(with post: Post) -> PostViewModel {
        let viewModel = PostViewModel(post, useCase: favouriteUseCase)

        viewModel.$isFavourite
            .filter { [weak self] in
                $0 == false && self?.showAll == false
            }
            .sink { [weak self] _ in
                self?.showFavouritePosts()
            }
            .store(in: &fetchPostCancel)

        return viewModel
    }

    func showFavouritePosts() {
        showAll = false
    }

    func showAllPosts() {
        showAll = true
    }

    func makeCommentViewModel(with post: PostViewModel) -> CommentsViewModel {
        .init(postViewModel: post, useCase: postsUseCase)
    }
}
