//
//  PostViewModel.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import Combine
import Foundation

final class PostViewModel: ObservableObject, Identifiable {
    let id: String
    let title: String
    let body: String
    @Published var isFavourite: Bool
    var cancel: AnyCancellable?

    init(_ post: Post, useCase: FavouriteUseCase) {
        id = "\(post.id)"
        title = post.title
        body = post.body
        isFavourite = useCase.isFavourite(post)
        cancel = $isFavourite
            .dropFirst()
            .removeDuplicates()
            .sink { [post, useCase] isFavourite in
                if isFavourite {
                    useCase.save(post)
                } else {
                    useCase.remove(post)
                }
            }
    }
}
