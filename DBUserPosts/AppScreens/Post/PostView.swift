//
//  PostView.swift
//  DBUserPosts
//
//  Created by hrasheed on 15.02.23.
//

import SwiftUI

struct PostView: View {
    @ObservedObject var viewModel: PostViewModel

    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading) {
                Text(viewModel.title)
                    .foregroundColor(Color.black)
                    .font(.headline)

                Text(viewModel.body)
                    .foregroundColor(Color.black)
                    .font(.subheadline)
            }

            Spacer()

            Button("Favourite_Button_Title") {
                viewModel.isFavourite.toggle()
            }
            .buttonStyle(makeButtonStyle())
            .eraseToAnyView()
        }
    }

    private func makeButtonStyle() -> any ButtonStyle {
        if viewModel.isFavourite {
            return FavouriteButtonStyle()
        }

        return UnfavouriteButtonStyle()
    }
}

struct FavouriteButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(8)
            .background(Color.blue)
            .foregroundColor(.white)
            .clipShape(Rectangle())
    }
}

struct UnfavouriteButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(8)
            .foregroundColor(.blue)
            .overlay(
                Rectangle()
                    .stroke(Color.blue, lineWidth: 1)
            )
    }
}

#if DEBUG
    struct PostView_Previews: PreviewProvider {
        static var previews: some View {
            PostView(viewModel: .init(.init(id: 1, userId: 2, title: "title.1", body: "body.1"), useCase: FavouriteLocalUseCase()))
        }
    }
#endif
