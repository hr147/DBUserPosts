# DBUserPosts

This application has been implemented using SwiftUI + MVVM

## Features

- [x] Show login screen
- [x] Show user's posts
- [x] User can favourite/unfavourite any post. 
- [x] User can apply filters e.g show all posts or favourite posts
- [x] User can see post's comments 

## View States
<table>
  <tr>
    <td>Login</td>
    <td>Posts</td>
    <td>Favourites</td>
    <td>Comments</td>
  </tr>
  <tr>
    <td><img src="ScreenShots/login.png" alt="Error State" width="200"/></td>
    <td><img src="ScreenShots/posts.png" alt="Error State" width="200"/></td>
    <td><img src="ScreenShots/favourites.png" alt="Error State" width="200"/></td>
    <td><img src="ScreenShots/comments.png" alt="Error State" width="200"/></td>
  </tr>
 </table>

## Technical Details

- [x] Created reusable independent/generic modules
- [x] Create UIs using SwiftUI
- [x] MVVM 
- [x] Use of async/await for networking calls

## Requirements

- iOS 14+ 
- Xcode 14+
- Swift 5.5+
- SPM

